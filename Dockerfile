FROM ubuntu:latest

RUN apt-get update ;\
apt-get install -y python \
sendmail \
python-pip \
software-properties-common \
git ;\
apt-add-repository -y ppa:ansible/ansible ;\
apt-get install sshpass ;\
pip install ansible pysphere requests pyvmomi;\
pip install awscli --upgrade;\
pip install paramiko

RUN git clone https://gitlab.com/osnexus/sos-ci.git 

ENV vsphere_host=VSPHERE_HOST_IP
ENV vsphere_user=VSPHERE_USER
ENV vsphere_password=VSPHERE_PASSWORD
ENV esxi_host=VSPHERE_ESXI
ENV datacenter=VSPHERE_DATACENTER 
ENV datastore=VSPHERE_DATASTORE
ENV aws_access_key=AWS_ACCESS_KEY
ENV aws_secret_key=AWS_SECRET_KEY
ENV aws_region=AWS_REGION
ENV vm_name=INSTANCE_NAME
ENV template_pass=TEMPEST_INSTANCE_PASSWORD
ENV sos_ci_home_dir=HOME_DIR
ENV sos_ci_ansible_dir=ANSIBLE_DIR
ENV sos_ci_repo_location=REPO_LOCATION
ENV sos_ci_log_dir=LOG_DIR
ENV stage=STAGE
ENV test_it_patch_num=TEST_IT_PATCH_NUM
RUN mkdir -p /.ssh

ADD gerrit_pub_key /.ssh/id_rsa.pub
ADD gerrit_prvt_key /.ssh/id_rsa

ADD gerrit_pub_key /root/.ssh/id_rsa.pub
ADD gerrit_prvt_key /root/.ssh/id_rsa

CMD ["bash", "sos-ci/start-ci.sh"]
