#!/bin/bash

mkdir sos-data

cd sos-ci
./set-env-to-vars.sh

COUNTER=0

# Loop until ps aux | grep -> exit code 0 """sendmail: MTA: accepting connections"""
# sleep every 2 seconds, after 50 counts, re-execute /etc/init.d/sendmail start &
# after 300 counts, exit with 1 if grep still returns null

command="/etc/init.d/sendmail start &"
grep_command="ps aux | grep 'sendmail: MTA: accepting connections'"

while [ $COUNTER -lt 300 ]
do
    if [ $COUNTER -eq 0 ] || [ $(($COUNTER % 50)) -eq 0 ]; then
        $command
    fi
    eval $grep_command
    if [ $? -eq 0 ]; then
        break
    else
        COUNTER=$((COUNTER+1))
        echo "Waiting for sendmail to start - Counter: $COUNTER"
        sleep 2
    fi
    if [ $COUNTER -eq 299 ]; then
        echo "FATAL ERROR: SENDMAIL FAILED TO START"
        exit 1
    fi
done

# os_ci is the main sos-ci code which will run the entire pipeline
# this will listen to gerrit event stream and put the patchsets to the queue and perform tempest testing on them

if [ ${#test_it_patch_num} -gt 1 ]
then
    python sos-ci/testit.py  $test_it_patch_num /sos-data
else
    python sos-ci/os_ci.py
fi


# testit.py is for testing purpose
# this will run the entire process(install devstack, run tempest, publish logs) with one patchset that has been pushed to gerrit
# patchset is of the form 'refs/changes/26/111226/3'
# results_dir is of the form '/home/user/test-dir'
# python sos-ci/testit.py  <ref-patchset-num> <result-dir-location>


#python sos-ci/testit.py  refs/changes/03/617503/11 /sos-data

# Use in debug mode to keep container alive after any failure for RCA
if [ -n "$STAGE" ]
then tail -f /dev/null
fi