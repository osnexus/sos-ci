#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "IP address of tempest machine required"
   exit 1
fi

ip="$1"

sshpass -p 'tempest@123' ssh-copy-id -f -i '/root/.ssh/id_rsa.pub' -o StrictHostKeyChecking=no tempest@$ip

if [ "$2" == "stack" ];then
    ssh tempest@$ip "tail -f -n 1000 /tmp/stack.sh.log.out"
else
    ssh tempest@$ip "tail -f -n 1000 /opt/stack/tempest/console.log.out"
fi

