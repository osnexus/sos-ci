#!/bin/bash

docker ps -a | grep sos-ci-container
if [ $? == 0 ]; then
    docker rm -f sos-ci-container
    docker rmi sos-ci
fi

echo $PUBLIC_KEY  >  gerrit_pub_key
echo "-----BEGIN RSA PRIVATE KEY-----" > gerrit_prvt_key
echo $PRIVATE_KEY >>  gerrit_prvt_key
echo "-----END RSA PRIVATE KEY-----" >> gerrit_prvt_key

chmod 600 gerrit_prvt_key
chmod 600 gerrit_pub_key

./set-env-to-vars.sh

docker build --no-cache -t sos-ci  --memory-swap -1 . 

#docker build -t sos-ci .

docker run -d --name sos-ci-container sos-ci

