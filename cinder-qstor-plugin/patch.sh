#!/bin/bash

mkdir -p cinder/tests/unit/volume/drivers
mkdir -p cinder/volume/drivers
cp quantastor.py cinder/volume/drivers
cp quantastor_api.py cinder/volume/drivers
cp test_quantastor.py cinder/tests/unit/volume/drivers
git diff /dev/null cinder/tests/unit/volume/drivers/test_quantastor.py > test.patch
git diff /dev/null cinder/volume/drivers/quantastor.py > q.patch
git diff /dev/null cinder/volume/drivers/quantastor_api.py > api.patch
mv test.patch ../cinder
mv q.patch ../cinder
mv api.patch ../cinder
rm -rf cinder
cd ../cinder
rm cinder/tests/unit/volume/drivers/test_quantastor.py
rm cinder/volume/drivers/quantastor.py
rm cinder/volume/drivers/quantastor_api.py
git apply --ignore-space-change --ignore-whitespace q.patch
git apply --ignore-space-change --ignore-whitespace test.patch
git apply --ignore-space-change --ignore-whitespace api.patch
rm q.patch
rm test.patch
rm api.patch
