## QuantaStor's Cinder Driver 

QuantaStor's Cinder driver provides iSCSI block storage to OpenStack managed virtual machines. Testing of the Quantastor device is done using tempest on a DevStack based OpenStack environment. 

Resulting that QuantaStor's Cinder driver is compatible with OpenStack's latest maintained version i.e. Rocky 

### Setup the Environment 

In order to setup the environment of DevStack which will be using Quantstor Cinder driver as a backend storage, perform the following steps

1. To setup DevStack, refer to [this link](https://docs.openstack.org/devstack/latest/)

2. After setup, add enabled_backends = quantastor and add the configuration (qs_ip, qs_pool_id, qs_user, qs_password) in /etc/cinder/cinder.conf like this at the end of the file 

   [quantastor]
   volume_driver=cinder.volume.drivers.quantastor.QuantaStorDriver
   volume_group = cinder-volumes
   iscsi_protocol = iscsi
   iscsi_helper = tgtadm
   san_ip = <quantastor_ip>
   qs_pool_id = <quantastor_pool_id> 
   san_user = <quantastor_username>
   san_password = <quantastor_pool_id>
   volume_backend_name = quantastor

3. Restart Cinder Service

### Feature Support

QuantaStor Cinder Driver supports the following features for the Openstack Environment

1. Volume Attach/Detach to Instances
2. Snapshot Create/Delete
3. Create Volume from Snapshot
4. Get Volume Stats
5. Copy Image to Volume
6. Copy Volume to Image
7. Clone Volume
8. Extend Volume

To check cinder logs:
sudo journalctl -q -f --unit devstack@c-vol

To restart cinder service:
sudo service devstack@c* restart