================================
QuantaStor Storage volume driver
================================

QuantaStor Storage fully integrates with the OpenStack platform through
the QuantaStor Cinder driver, allowing a host to configure and manage
QuantaStor Storage array features through Block Storage interfaces.

QuantaStor's Cinder driver provides iSCSI block storage to OpenStack managed
virtual machines.

Supported operations
~~~~~~~~~~~~~~~~~~~~

* Create, delete, clone, attach, and detach volumes
* Create and delete volume snapshots
* Create a volume from a snapshot
* Copy an image to a volume
* Copy a volume to an image
* Extend a volume
* Get volume statistics
* Manage and unmanage a volume


QuantaStor Storage driver configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Update the file ``/etc/cinder/cinder.conf`` with the given configuration.

In case of multiple back-end configuration, for example, configuration
which supports multiple or a single QuantaStor Storage array
with arrays from other vendors, use the following parameters.

In case of single back-end configuration of QuantaStor Storage driver.

.. code-block:: ini

   enabled_backends = quantastor (in [DEFAULT] section)

   [quantastor]
   volume_driver=cinder.volume.drivers.quantastor.QuantaStorDriver
   volume_group = cinder-volumes
   iscsi_protocol = iscsi
   iscsi_helper = tgtadm
   qs_ip = <quantastor_ip>
   qs_pool_id = <quantastor_pool_id>
   qs_user = <quantastor_username>
   qs_password = <quantastor_pool_id>
   volume_backend_name = quantastor

In case of multiple back-end configuration of QuantaStor Storage driver.

.. code-block:: ini

   enabled_backends = quantastor_pool_1, quantastor_pool_2 (in [DEFAULT]
   section)

   [quantastor_pool_1]
    (same configuration as above)

   [quantastor_pool_2]
    (same configuration as above)

.. code-block:: console

   $ openstack volume type create QuantaStor_VOLUME_TYPE
   $ openstack volume type set --property
   volume_backend_name=QuantaStor_BACKEND_NAME QuantaStor_VOLUME_TYPE

.. note::

   Restart the ``cinder-api``, ``cinder-scheduler``, and ``cinder-volume``
   services after updating the ``cinder.conf`` file.

Configure the cluster
~~~~~~~~~~~~~~~~~~~~~

If a cluster is used as the cinder storage resource, the following verification
is required on QuantaStor Storage Appliance:

  * Verify that the qs_ip and storage pool belong to the same head.