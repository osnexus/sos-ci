#!/bin/bash

conf_file="sos-ci.yaml"
password_file="sos-ci/ansible/password.txt"
dockerfile="Dockerfile"
creating_template="create-template.sh"
creating_sos_vm="create-sos-vm.sh"
sos_ci_vars="sos-ci/ansible/group_vars/all.yml"

f_sed() {
          sed -i "s|$1|$2|g"  $3 > /dev/null 2>&1
        }
set "$conf_file" "$password_file" "$dockerfile" "$creating_template" "$creating_sos_vm" "$sos_ci_vars"
for i
do
f_sed VSPHERE_HOST_IP $vsphere_host $i
f_sed VSPHERE_USER $vsphere_user $i
f_sed VSPHERE_PASSWORD $vsphere_password $i
f_sed VSPHERE_ESXI $esxi_host $i
f_sed VSPHERE_DATACENTER $datacenter $i
f_sed VSPHERE_DATASTORE $datastore $i
f_sed AWS_ACCESS_KEY $aws_access_key $i
f_sed AWS_SECRET_KEY $aws_secret_key $i
f_sed AWS_REGION $aws_region $i
f_sed INSTANCE_NAME $vm_name $i
f_sed TEMPEST_INSTANCE_PASSWORD $template_pass $i
f_sed LOCALHOST_PASS $localhost_pass $i
f_sed HOME_DIR $sos_ci_home_dir $i
f_sed ANSIBLE_DIR $sos_ci_ansible_dir $i
f_sed REPO_LOCATION $sos_ci_repo_location $i
f_sed LOG_DIR $sos_ci_log_dir $i
f_sed STAGE $stage $i
f_sed TEST_IT_PATCH_NUM $test_it_patch_num $i
done
