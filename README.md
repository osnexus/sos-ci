<strong>OSNEXUS-Quantastor-sos-ci - Simple OpenStack Continuous Integration </strong>
================================================

This is a simple app to let you implement a third-pary CI setup for Openstack in your own lab, which is needed to get our Quantastor Cinder Volume Plugin get accepted by Openstack Community

Third Party CI =>  which will run tempest testing against gerrit event stream. Using this event stream, it is possible to test commits against testing system. It is also possible for these CI's to feed information back into Gerrit and they can also leave non-gating votes on Gerrit review requests.
There are many CI's that read the Gerrit event stream and run their own tests on the commits. For each patch set the third party CI tests, then adds a comment in Gerrit with a summary of the test result and links to the test artifacts.

### Infrastructure Requirements:

1. VCenter server with API access
2. Unattended installation ISO for Ubuntu 16.04 (which will be needed to create vm on vcenter without manual intervention)
3. Ubuntu VM (which will run Jenkins, that has all the jobs to run containerized sos-ci)
4. A dedicated Gerrit CI account
   1. Create this account on https://launchpad.net/
   2. The name should have three pieces Organization Product/technology CI (OSNEXUS QUANTASTOR CI)
   3. Set username on https://review.openstack.org/#/settings/
   4. Add the SSH public key, you will be using (which will be present on sos-ci machine through command ssh-keygen) to the Gerrit account (as of now we have added that keys (public and private both) to jenkins environment variables)

## <strong> Steps to run sos-ci </strong>

1. On the Fresh Ubuntu Instance, pull osnexus/sos-ci repository (https://gitlab.com/osnexus/sos-ci).
2. Run bootstrap-jenkins-container script in sos-ci directory with jenkins-ci-setup.tar's path (which has been shared through google drive(download it and place it on the vm's home directory)) as an arugument. This script will pull jenkins image and place the jenkins-ci-setup.tar (archive of Jenkins container volume) to the container's persistent volume. <br />
    ```shell 
    bash bootstrap-jenkins-container jenkins-ci-setup.tar
    ```
3. Once done, run the jenkins's image using the below command

    ```shell
    sudo docker run -d -p 8080:8080 \
    --name jenkins \
    --mount source=jenkins-sos-ci-volume,target=/var/jenkins_home
    jenkins/jenkins
    ```

4. Login jenkins with localhost:8080 or <IP>:8080 with user name = ..... and password = ..... 

5. Run the bulid-tempest-template job, after the successful completion of bulid-tempest-template job, run the [dev/prod]-sos-ci job.

## <strong> Description of bulid-tempest-template job </strong>

This job is pulling OSNEXUS/sos-ci gitlab repo and running create-template.sh script which is creating a virtual machine on vmware and converting it into a template so that for every patch-set coming into the queue from gerrit event stream, a vm can be cloned from that template, to run tempest on it for that patch-set.

## <strong> Description of  run-prod-sos-ci job </strong>

This job is pulling OSNEXUS/sos-ci gitlab repo and then running run-docker-container.sh bash script, which is containerizing the entire python application having entry point as start-ci.sh which again is a script, which is running os-ci.py code (that is listening to gerrit-event-stream for all incoming cinder patches and adding the patches into queue)

os-ci.py is a base class which is calling executor.py's method (which is running the entire lifecycle of setting up the devstack setup and running tempest testing, then uploading logs, onto one patchset picked up from queue)

executor.py is invoking ansible-playbooks or roles to complete one life cycle, i.e.

1. clean-all-vms.yml is getting all the list of virtual machines in v-center and invoking clean_up_vmware.yml,

   to delete all the vms, that has been created previously for cinder patchset, and has not been deleted due to any breakdown in the flow

2. clone-vm.yml clones a virtual machine for tempest testing in v-center with patch-set reference name.

3. run-ci.yml which has a role: devstack

   1. install_devstack.yml - install devstack on the newly cloned vm with patch-set ref as a branch 
   2. run_tempest.yml - run tempest on that branch
   3. run_cleanup.yml - collects the logs, makes a tar file of the logs and send it to the log directory of the container or the vm which is running the pipeline 

4. publish.yml which has a role: publish

   publish_logs.yml - untar the logs from the log directory and put it to the aws bucket

5. teardown.yml deleting the virtual machine created for one patchset ref in v-center and deleting its entry from the ansible hosts inventory

   ## <strong> Shell Scripts description, used to automate jobs </strong>

   ### run-docker-container.sh

   This script is checking if there is any previous sos-ci docker image created and deleteing it and then building 

   a docker image of sos-ci python application and running it in a container.

   ### set-env-to-vars.sh 

   It is picking all the enviornment variables from jenkins and writing it to variables inventory, which will be used by ansible.

   ### create-sos-vm.sh

   It will create a virtual machine to run sos-ci onto it, but for now we are running sos-ci on a docker ubuntu container

   ### create-template.sh

   It is creating a vm on vcenter and coverting it into a template, that template is used to clone a new vm for every incoming patchset

   ### start-ci.sh

   It is starting the sos-ci pipeline and this is the entry point in Dockerfile 

   ### watch-tempest-logs.sh

   This scripts will show the logs of devstack setup and tempest tests of patchset ref vm which is created of tempest testing by providing the ip of the vm. This is for develeopment purpose

- ### Requirements : These are configured in Dockerfile, as we are running a sos-ci container

  ansible <br />
  iniparse <br />
  paramiko>=1.13.0 <br />
  scp>=0.8.0 <br />
  subunit2sql <br />
  aws <br />
  boto3 <br />
  awscli==1.6.6 <br />
  botocore <br />

  #### If you want to contribute the code

- **Git Clone**

  ```https://gitlab.com/osnexus/sos-ci```

- ### To run sos-ci in a virtual-machine, edit sos-ci.yaml according to the parameters mentioned

## <strong> Environment Variables Description</strong>

    vsphere_host = vsphere host IP address or FQDN
    vsphere_user = vsphere username
    vsphere_password = vsphere password
    exsi_host = vsphere esxi IP
    datacenter = vsphere datacenter name
    datastore = vsphere datastore name 
    aws_access_key = access key of aws 
    aws_secret_key = secret key of aws 
    aws_region = aws region
    localhost_pass = ubuntu instance password, which is running the jenkins
    template_pass = password of vm that has been created into a template
    vm_name = name of the vm, we are creating to convert to a template
    PRIVATE_KEY = gerrit account private key
    PUBLIC_KEY = gerrit account public key
    sos_ci_repo_location = location where sos-ci git repo is pulled, if in docker conatiner, it will be simply /sos-ci, if in a vm we are running sos-ci, it will be diff like /home/user/sos-ci
    sos_ci_log_dir = log dir location, in vm => /home/user/sos-data, in a docker container => /sos-data
    sos_ci_home_dir = home-dir where sos-ci is running, in vm => /home/user, in a docker container=> /
    sos_ci_ansible_dir = location of ansible dir in sos-ci repo, in a vm => /home/user/sos-ci/sos-ci/ansible, in a docker container => /sos-ci/sos-ci/ansible

