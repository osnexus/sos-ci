#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
else
    if [ "$USER" != "quantastor-ci" ]; then
        grep "/bin/bash" /etc/passwd | cut -d: -f1 | grep -q quantastor-ci
        if [ $? != 0 ]; then
            adduser --quiet --disabled-password --shell /bin/bash --home /home/quantastor-ci --gecos "User" quantastor-ci
            echo "quantastor-ci:master#123" | sudo chpasswd
        fi
    fi
    echo 'quantastor-ci  ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers.d/quantastor-ci
    su - quantastor-ci -c "sudo bash /home/$1/sos-ci/start-ci.sh $1"
fi

#sudo bash  initial-setup-ci.sh $USER
