#!/bin/bash
#
# Description:
# This script is for setup the jenkins dashboard.
#
#------------------------------
# GLOBAL VARIABLES
#------------------------------
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
else
    if [ "$#" -eq 0 ]; then
        echo "enter tar volume path"
        exit 1
    fi

    VOLUMES_DIR=/var/lib/docker/volumes
    VOLUME_NAME=jenkins-sos-ci-volume

    #----------------------------------------------------
    # Installing docker package and pulling jenkins image
    #----------------------------------------------------

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    apt-cache policy docker-ce
    sudo apt-get install -y docker-ce

    #---------------------------------------------
    # Creating a new volume and untar given volume 
    #---------------------------------------------

    if [ $? -eq 0 ]; then
        sudo docker volume create jenkins-sos-ci-volume
    fi
    sudo docker pull jenkins/jenkins

    cp $1 $VOLUMES_DIR/$VOLUME_NAME
    cd $VOLUMES_DIR/$VOLUME_NAME
    tar -xvzf $1

    #------------------------------------------
    # Move _data folder to newly created volume 
    #------------------------------------------

    mv jenkins-ci-setup/_data/* _data/
fi
