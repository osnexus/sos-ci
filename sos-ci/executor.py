import os
import subprocess
import time

import log

from yaml import load

from os_ci import event_queue

fdir = os.path.dirname(os.path.realpath(__file__))
conf_dir = os.path.dirname(fdir)
with open(conf_dir + '/sos-ci.yaml') as stream:
    cfg = load(stream)

debug_value = (os.environ['stage'])

# Misc settings
DATA_DIR =\
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/data'
if cfg['Data']['data_dir']:
    DATA_DIR = cfg['Data']['data_dir']

logger = log.setup_logger(DATA_DIR + '/ansible.out')

if (debug_value == 'deployment'):
    cmd = 'ansible-playbook --extra-vars '\
        '\"ansible_sudo_pass=%s\" %s/clean-all-vms.yml' % (cfg['Tempest_instance']['password'], cfg['Ansible']['ansible_dir'])
    logger.debug('Running ansible clear-vmware command ONCE: %s', cmd)
    ansible_proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    result = ansible_proc.communicate()[0]
    logger.debug('Response from ansible: %s', result)

""" EZ-PZ just call our ansible playbook.

Would be great to write a playbook runner at some point, but
this is super straight forward and it works so we'll use it for now.
"""

def just_doit(patchset_ref, results_dir):
    """ Do the dirty work, or let ansible do it. """

    ref_name = patchset_ref.replace('/', '-')
    logger.debug('Attempting ansible tasks on ref-name: %s', ref_name)
    vars = "instance_name=%s" % (ref_name)
    vars += " patchset_ref=%s" % patchset_ref
    vars += " results_dir=%s" % results_dir
    vars += " home_dir=%s" % cfg['Home']['dir']
    vars += " current_dir=%s" % cfg['Current-working-dir']['dir']

    cmd = 'ansible-playbook --extra-vars '\
          '\"ansible_sudo_pass=%s %s\" %s/clone_vm.yml' % (cfg['Tempest_instance']['password'], vars, cfg['Ansible']['ansible_dir'])
    logger.debug('Running ansible clone_vm command: %s', cmd)
    ansible_proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    result = ansible_proc.communicate()[0]
    logger.debug('Response from ansible: %s', result)

    cmd = 'ansible-playbook -i %s/sos-ci/ansible/hosts --extra-vars '\
          '\"ansible_sudo_pass=%s %s\" %s/run_ci.yml' % (cfg['Current-working-dir']['dir'], cfg['Tempest_instance']['password'], vars, cfg['Ansible']['ansible_dir'])

    logger.debug('Running ansible run_ci command: %s', cmd)
    ansible_proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output = ansible_proc.communicate()[0]
    logger.debug('Response from ansible: %s', output)
    
    if debug_value == 'deployment':
        cmd = 'ansible-playbook --extra-vars '\
            '\"ansible_sudo_pass=%s %s\" %s/publish.yml' % (cfg['Tempest_instance']['password'], vars, cfg['Ansible']['ansible_dir'])
        logger.debug('Running ansible publish command: %s', cmd)

        # This output is actually the ansible output
        # should fix this up and have it just return the status
        # and the tempest log that we xfrd over
        ansible_proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output += ansible_proc.communicate()[0]
        logger.debug('Response from ansible: %s', output)


    success = False
    hash_id = None
    rerun = False
    console_log = results_dir + '/' + ref_name + '/' + 'console.log.out'
    logger.debug('Looking for console log at: %s', console_log)
    if os.path.isfile(console_log):
        logger.debug('Found the console log...')
        if 'Failed: 0' in open(console_log).read():
            logger.debug('Evaluated run as successful')
            success = True
        elif ('Volume Type luks already exists' in open(console_log).read() or 'failed to build and is in ERROR status' in open(console_log).read()
              or 'failed to reach available status' in open(console_log).read()):
                rerun = True

        logger.info('Status from console logs: %s', success)
        # We grab the abbreviated sha from the first line of the
        # console.out file
        with open(console_log) as f:
            first_line = f.readlines()
        print "Attempting to parse: %s" % first_line
        first_line=first_line[1]
        hash_id = first_line.split()[1]


    # Finally, delete the instance regardless of pass/fail
    # NOTE it's moved out of tasks here otherwise it won't
    # run if preceeded by a failure

    cmd = 'ansible-playbook --extra-vars '\
          '\"%s\" %s/teardown.yml' % (vars, cfg['Ansible']['ansible_dir'])

    logger.debug('Running ansible teardown command: %s', cmd)
    ansible_proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output += ansible_proc.communicate()[0]

    return hash_id, success, output, rerun
