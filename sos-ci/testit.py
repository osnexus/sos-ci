#!/usr/bin/python

import os
import sys
import time
import executor
from os_ci import JobThread

jobthread = JobThread()
# patchset is of the form 'refs/changes/26/111226/3'
# results_dir is of the form '/home/user/test-dir'

patchset = str(sys.argv[1])
results_dir = str(sys.argv[2])
review_num = patchset.split("/")[3]

ref_name = patchset.replace('/', '-')

# build the dir
results_dir = results_dir + '/' + ref_name
os.mkdir(results_dir)

(commit_id, success, results, rerun) = executor.just_doit(patchset, results_dir)
print "hash_id:%s", commit_id
print "success:%s", success
print "result:%s", results
print "reruns:%s", rerun

if rerun == False and commit_id is not None:
    time.sleep(199)
    log_location = "http://openstack-ci.osnexus.net.s3.us-east-1.amazonaws.com/index.html?prefix=%s"%(ref_name)
    review_url = "https://review.openstack.org/%s"%(review_num)

    (subject, message) = jobthread._post_results_to_gerrit(log_location, success, commit_id, review_url)
    subject += review_url
    jobthread._send_notification_email(subject, message)